#FROM resin/armv7hf-debian
FROM arm32v7/python

COPY ./qemu-arm-static /usr/bin/qemu-arm-static

#RUN apt-get update
#RUN apt-get install python3 wget gcc python3-dev libpq-dev
#RUN rm -rf /var/lib/apt/lists/*
#RUN wget -O get-pip.py 'https://bootstrap.pypa.io/get-pip.py'
#RUN python3 get-pip.py --disable-pip-version-check --no-cache-dir 
RUN pip install uwsgi django redis envparse psycopg2 celery
RUN cd /srv && django-admin startproject www
#COPY ./src /srv/www
RUN groupadd -r uwsgi --gid 999
RUN useradd -r -g uwsgi --uid 999 uwsgi
RUN chown -R uwsgi:uwsgi /srv/www
ENTRYPOINT ["uwsgi", "--uid", "999", "--gid", "999", "--chdir", "/srv/www", "--wsgi-file", "www/wsgi.py", "--env", "DJANGO_SETTINGS_MODULE=www.settings", "--http", ":8000", "--thunder-lock", "--enable-threads", "--processes", "4", "--threads", "2", "--offload-threads", "4"]

